/********************
Olivia Gurrie
ogurrie
Lab 5
Lab Section: 006
Anurata Hridi
********************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

   // Initializes the deck of cards
   Card deck[52];
   for(int i = 0; i < 13; i++){
      deck[i].suit = SPADES;
      deck[i].value = i+2;
   }
   for(int i = 13; i < 26; i++){
      deck[i].suit = HEARTS;
      deck[i].value = i-11;
   }
   for(int i = 26; i < 39; i++){
      deck[i].suit = DIAMONDS;
      deck[i].value = i-24;
   }
   for(int i = 39; i < 52; i++){
      deck[i].suit = CLUBS;
      deck[i].value = i-37;
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   // Randomly sorts the deck of cards
   random_shuffle(&deck[0], &deck[52], myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

   // Selets five cards from the top of the deck
   Card hand[5] = { deck[0], deck[1], deck[2], deck[3], deck[4] };

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

   // Sorts the hand in order
   sort(&hand[0], &hand[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

   // Prints out the cards in order
   for(int i = 0; i < 5; i++){
     cout << setw(10) << right << get_card_name(hand[i]) << " of "
          << get_suit_code(hand[i]) << endl;
   }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

// The function for sorting the hand
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit == rhs.suit){
    if(lhs.value < rhs.value){
      return true;
    }
    else{
      return false;
    }
  }
  if(lhs.suit < rhs.suit){
    return true;
  }
  else{
    return false;
  }
}

// The function for getting the card symbol
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

// The function for getting the card number/face value
string get_card_name(Card& c) {
  switch (c.value) {
    case 2:  return "2";
    case 3:  return "3";
    case 4:  return "4";
    case 5:  return "5";
    case 6:  return "6";
    case 7:  return "7";
    case 8:  return "8";
    case 9:  return "9";
    case 10: return "10";
    case 11: return "Jack";
    case 12: return "Queen";
    case 13: return "King";
    case 14: return "Ace";
    default: return "";
  }
}
